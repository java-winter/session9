package compare;

class Employee{
	String name;
	double wage;
	
	public Employee(String name, double wage) {
		this.name = name;
		this.wage = wage;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", wage=" + wage + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	

	
	
}
public class App {
	public static void main(String[] args) {
		//I would like to compare two employee
		
		Employee emp1 = new Employee("A", 1200);
		Employee emp2 = new Employee("A", 1200);
		
		System.out.println(emp1);
		System.out.println(emp2);
		
		if(emp1 == emp2) {
			System.out.println("they are the same");
		}else {
			System.out.println("they are not the same");
		}
		
		if (emp1.equals(emp2)) {
			System.out.println("they are the same");
		}else {
			System.out.println("they are not the same");
		}
		
		String str1= new String("Montreal");
		String str2= new String("Montreal");
		
		if (str1.equals(str2)) { //str1 == str2 => is wrong
			System.out.println("they are the same");
		}else {
			System.out.println("they are not the same");
		}
		
		String str3= new String("Montreal");
		String str4= str3;

		if (str3 == str4) {
			System.out.println("they are the same");
		}else {
			System.out.println("they are not the same");
		}
	}
}
