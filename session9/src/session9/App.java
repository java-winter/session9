package session9;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class App {

	public static void main(String[] args) {
		//saving some texts in a file
		try {
			fileWriterExample();
			fileWriteWithPathExample();
			
			fileReaderExample();
			fileReadWithPathExample();
		}catch(IOException exc) {
			System.out.println("an error happened for writing to the file" + exc.getMessage());
		}
	}
	
	
	private static void fileWriterExample() throws IOException {
		String fileName = "src/resource/testfile";
		//try with resource		
		try(FileWriter fileWriter = new FileWriter(fileName, true)) {
			PrintWriter printer = new PrintWriter(fileWriter);			
			printer.println("this is my first line");
			printer.printf("my product name is %s and its price is %d", "iphone", 2000);
			printer.println();
		} catch (IOException e) {
			//System.out.println(e.getMessage());
			throw e;
		}
		System.out.println("using printwriter finished");
	}
	
	
	private static void fileReaderExample() throws IOException {
		String fileName = "src/resource/testfile";

		try(FileReader reader = new FileReader(fileName)){
			int data = reader.read();
			while(data != -1) {
				System.out.println((char)data);
				data =reader.read();
			}
			System.out.println("");
		}
		catch(IOException exc) {
			throw exc;
		}
	}
	
	
	
	
	//good approach
	private static void fileWriteWithPathExample() throws IOException {
		String fileName = "src/resource/testfile2";
		String msg= "this is my message";
		
		Path path = Paths.get(fileName);
		
		byte[] msgToByte = msg.getBytes();
		
		try {
			//Files.write(Paths.get(fileName), msg.getBytes());
			Files.write(path, msgToByte);
		} catch (IOException e) {
			throw e;

		}
		System.out.println("using filewrite finished");
	}
	
	private static void fileReadWithPathExample() throws IOException {
		String fileName = "src/resource/testfile2";
		Path path = Paths.get(fileName);
		List<String> lines;
		try {
			lines = Files.readAllLines(path);
			
			for(int i=0; i< lines.size(); i++) {
				System.out.println(lines.get(i));
			}
			
			for (String line : lines) {
				System.out.println(line);
			}
			
			//Lambda expression in Java 8
			lines.forEach(l -> System.out.println(l));
			
		} catch (IOException e) {
			throw e;
		}
		
			
	}
	
	
	
}
