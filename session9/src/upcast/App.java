package upcast;

abstract class Animal{
	abstract void sound();
}

class Dog extends Animal{

	@Override
	void sound() {
		System.out.println("WOOF woof");		
	}
	
	void cathBall() {
		System.out.println("catch ball");
	}
}

class Cat extends Animal{

	@Override
	void sound() {
		System.out.println("meuex meux");
	}
	
	void scratch() {
		System.out.println("scratching");
	}
}

public class App {
	
	public static void main(String[] args) {
		
		Animal cat = new Cat();//upcasting
		Animal dog = new Dog();
		
		doSound(cat);
		doSound(dog);
		
		
		//DownCasting
		Animal animal1 = new Cat();
		doAnimalSpecificThings(animal1);
		
		Animal animal2 = new Dog();
		doAnimalSpecificThings(animal2);
	}
	
	private static void doSound(Animal animal) {
		animal.sound();
	}

	private static void doAnimalSpecificThings(Animal animal) {
		
		animal.sound();
		
		if(animal instanceof Cat) {
			Cat specialCat = (Cat) animal; //downcasting
			specialCat.scratch();
		}
		if (animal instanceof Dog) {
			Dog specialDog = (Dog) animal;//downcasting
			specialDog.cathBall();
		}
		
	}
}
